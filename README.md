jQuery_navbar
=============

jQuery nav bar used for mobile devices and menu areas with limited space. 

Nav Bar created with jQuery and CSS styling that is made to restyle the nav bar after the screen reaches a 
certain resolution size. 
